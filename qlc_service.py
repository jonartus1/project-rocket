import subprocess
import time
import mqtt_config
import global_config
import signal
from heartbeat import Heartbeat


# Responsible for controlling the lifecycle of the QLC+ application. 
# This is separated as you need to start QLC before the lighting service in order to read the list of available scene/chase widgets.
class QlcService:
    def __init__(self):
        self.logger = mqtt_config.create_logger("service", "qlc")
        self.logger.info(f"Starting qlc service on {global_config.get_hostname()} with version {global_config.config['version']}")
                                                                             
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: 'running')
        self.proc = {}
        signal.signal(signal.SIGTERM, self.terminating)

    def run(self):
        command = "qlcplus -w -p -o ~/rocket/install/rocket_ship.qxw"
        self.proc = subprocess.Popen(command, shell=True)
        self.logger.info('Started QLC+ service.')

    def terminating(self, signum, frame):
        self.logger.info(f'Terminating QLC+ service after signal {signum}')
        self.proc.terminate()

if __name__ == "__main__":
    qlc = QlcService()
    qlc.run()

    while True:
        time.sleep(60)

