

// Create a client instance
client = new Paho.MQTT.Client("127.0.0.1", 9001, "Rocket-Web");

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

const handlers = {
    "admin/config": function (payload) {
        onConfigReceived(payload); 
    },
    "event/rfid/card_scanned": function (payload) {
        $("#scanned-uid").text(payload.uid);
    },
    "ship/main_lights/set": function (payload) {
        $("#ambient-r").text(payload.r);
        $("#ambient-g").text(payload.g);
        $("#ambient-b").text(payload.b);
    },
    "admin/heartbeat/#": function (payload) {
      if (payload.singleton){
        heartbeats[payload.service] = payload
      } else {

      }
    }
}

const heartbeats = {

}

function updateHeartbeats() {
  $.each(heartbeats, function(key, value) {
    let duration = Math.round(Date.now() / 1000 - value.timestamp);
    let target = $('#' + key + 'Heartbeat');

    if (target) {
      target.text(duration);

      if (duration < 10) {
        target.css('background-color', 'green');
        return;
      }

      if (duration < 30) {
        target.css('background-color', 'orange');
        return;
      }

      target.css('background-color', 'red');
    }
  });
}

setInterval(updateHeartbeats, 1000);

function onConnect() {
  console.log("onConnect");

  for (const [key, value] of Object.entries(handlers)) {
    console.log(`Subscribing to: ${key}`);
    client.subscribe(key);
  }

  publish('admin/config/request', { })
}

function onConfigReceived(payload) {
  console.log("onConfigReceived");

  var knownCards = payload.game_variables.known_uids;
  
  knownCards.forEach(card => {
    $("#security-uid").append($("<option />").val(card.id).text(card.name))
  });
    
}

function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

function onMessageArrived(message) {
  console.log("onMessageArrived:"+message.payloadString);
  console.log("Destination: " + message.destinationName);

  if (handlers.hasOwnProperty(message.destinationName)) {
    handlers[message.destinationName](JSON.parse(message.payloadString));  
  } else {
    $.each(handlers, function(key, value){
      if (key.endsWith('#')){
        if (message.destinationName.startsWith(key.replace('#', ''))){
          value(JSON.parse(message.payloadString));
        };
      }
    });
  }
}

// connect the client
//client.connect({userName: "rocket", password: "mttKsC8vgpYPHH", onSuccess:onConnect});
client.connect({onSuccess:onConnect});

function publish(topic, payload) {
  message = new Paho.MQTT.Message(JSON.stringify(payload));
  message.destinationName = topic;
  client.send(message);
}

$(document).ready(function() {
  $("#security-uid-button").click(function () {
    payload = {"uid": $("#security-uid").val()}
    publish("event/rfid/card_scanned", payload);
  }) 

  $("#ambient-r-button").click(function () {
    payload = { "type": "value_changed", "input": 10, "value": $("#ambient-r-test").val() * 1}
    publish("event/physical_input", payload);
  })
  
  $("#ambient-g-button").click(function () {
    payload = { "type": "value_changed", "input": 11, "value": $("#ambient-g-test").val() * 1}
    publish("event/physical_input", payload);
  })

  $("#ambient-b-button").click(function () {
    payload = { "type": "value_changed", "input": 12, "value":  $("#ambient-b-test").val() * 1}
    publish("event/physical_input", payload);
  })
});


