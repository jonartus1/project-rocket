import time
import mqtt_config
import global_config
from heartbeat import Heartbeat

class WledService:
    def __init__(self):
        self.logger = mqtt_config.create_logger("service", "wled")
        self.logger.info(f"Starting wled service on {global_config.get_hostname()} with version {global_config.config['version']}")
                                                                             
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: 'running')

    def run(self):
        self.logger.info('Started WLED service.')

    def terminating(self, signum, frame):
        self.logger.info(f'Terminating QLC+ service after signal {signum}')
        self.proc.terminate()

if __name__ == "__main__":
    wled = WledService()
    wled.run()

    while True:
        time.sleep(60)

