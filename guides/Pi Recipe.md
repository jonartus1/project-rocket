## Setup Instructions

1. Remove any entries from your ~/.ssh/known-hosts for the hostname
2. Create a clean Raspbian image for your Pi (Desktop Without Apps is fine - VNC is handy...)
3. (If using WiFi) Connect to Syzygy Corp 5G
4. Set the following preferences in 'Preferences -> Raspberry Pi Configuration'

- Hostname (Normally PI-xx)
- Interfaces -> Enable SSH, VNC, SPI, I2C
- Display -> Set Headless Resolution

CWD to .install and run ./pi-config.sh <hostname> (watch for prompts and enter passwords as necessary)
