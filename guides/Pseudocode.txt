
QLC-Lights 
    Scene: HouseLights, Standby, NormalRunning
    Chase: FlashRed

WLED-Lights 
    HouseLights, Standby, NormalRunning, BlackOut

RGB-LED


TTS


Background-Audio
    EmptyShip
    EngineNoise

FX-Audio
    Laser 0.5s
    Engine-Startup 3.9s

Beep-Audio





HouseLights
    QLC-Lights Scene HouseLights
    WLED-Lights HouseLights
    RGB-LED Solid 128, 128, 128
    TTS "Initialised"
    Background-Audio None


Standby
    QLC-Lights Scene Standby
    WLED-Lights Standby
    RGB-LED Pulse 128, 0, 0
    Background-Audio EmptyShip


NormalRunning
    QLC-Lights Scene NormalRunning
    WLED-Lights NormalRunning
    RGB-LED Pulse 64, 0, 192
    Background-Audio EngineNoise, 128


IdCardNotRecognised
    TTS "Id Card Not Recognised"
    QLC-Lights Chase FlashRed
    Beep-Audio LowD 1

IdCardAccepted (name)
    TTS "Id Card Accepted. Welcome $name"


BootSequence
    WLED-Lights BlackOut
    Fx-Audio


Module(IdScanner)
    ValidIds '884d01d41'

    OnAction('id_scanned', id =>
        If(GameState = Standby)
            If id in ValidIds
                Effect(IdCardAccepted)
                Game.Authenticate(ValidIds[id])
            Else   
                Effect(IdCardNotRecognised)
    )


Game
    Init HouseLights







Devices - Hardware
Services - one per facility above