TODO:

- Wire up physical input listener with new scheme
- Wire up admin commands to web buttons and change addressing scheme
- Wire up logout button to the web interface

All channels are prefixed by a global namespace, e.g. Syzygy, Rocket, etc.

Information is broadcast at three levels of granularity:

- rocket/system contains game-level concepts which would be meaningful to the player, e.g. Engine Throttle = 85%
- rocket/service contains logical information at python service level, including implementation details like state machine info or lighting preset names
- rocket/hardware contains physical details of the underlying hardware, e.g. uptime, temperature, memory usage, last heartbeat etc.

Commands are sent in the following channels:

- rocket/commands/admin contains environment-level commands for use outside of the game, e.g. to restart/update the services
- rocket/commands/service contains communications between services, e.g. to play a sound or a lighting chase

Events are recorded in:

- rocket/events/user for input from users, e.g. a button press or an rfid scan
