

QLC+ (DMX Lighting Control)
Install the Debian package from the repo install directory


MQTT Broker:
============
sudo apt install -y mosquitto mosquitto-clients
sudo systemctl enable mosquitto.service


You'll need to allow Websocket connections to the MQTT broker:
* Create a new file at /etc/mosquitto/conf.d/default.conf
* Add the following content:

listener 1883
protocol mqtt
allow_anonymous true

listener 9001
protocol websockets

* Restart mosquitto with sudo systemctl restart mosquitto


WLED
====
https://install.wled.me/


