* MQTT Topics *


Variables
=========
{DeviceId} - Every physical device has a unique device ID, e.g. "Ship-01", "Pi-01" or that sort of thing.


Admin
=====
Contains anything used to manage/monitor the various services.


admin/config/{DeviceId} - Every device should subscribe to this topic, to receive their config, and any subsequent updates.
admin/config/request => { "DeviceId": { DeviceId } } - Every device should publish to this topic on start-up until the main service replies with config.

admin/config - Services/Devices which require full config should subscribe to this channel.
admin/config/request - A request for the full config can be made by publishing an empty message to this channel

admin/heartbeat/device/{DeviceId} - Every device should publish a heartbeat at least every 10s to this channel.

admin/reset - Every device should subscribe to this channel and do a hard reset if a message is received.


Events
======

action/button/{ButtonName}