Download firmware: https://micropython.org/download/esp32/
pip install esptool
pip install adafruit-ampy

python -m esptool --chip esp32 --port com8 --baud 460800 write_flash -z 0x1000 esp32-20220618-v1.19.1.bin
ampy -p com8 put main.py

