import mqtt_config
import json

class PhysicalInputTrigger:
    def __init__(self):
        self.mqtt = mqtt_config.create_mqtt()

    def button_pushed(self, input, includes={}):
        payload = json.dumps({'type': 'button_pushed', 'input': input, 'includes': includes})
        print(payload)
        self.mqtt.publish('event/physical_input', payload)

    def input_value_changed(self, input, value, includes={}):
        payload = json.dumps({'type': 'value_changed', 'input': input, 'value': value, 'includes': includes})
        print(payload)
        self.mqtt.publish('event/physical_input', payload)
