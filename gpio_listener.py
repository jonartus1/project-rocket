import time
import global_config
import mqtt_config
import RPi.GPIO as GPIO

from physical_input_trigger import PhysicalInputTrigger
from heartbeat import Heartbeat
from collections import deque


class ButtonService:
    def __init__(self):
        self.config = global_config.get_service_config('gpio_listener')
        self.logger = mqtt_config.create_logger('gpio_listener')
        self.trigger = PhysicalInputTrigger()
        self.button_map = self.config['buttons']
        self.include_map = 'include_state' in self.config and self.config['include_state'] or {}
        self.config['samples'] = 'samples' in self.config and self.config['samples'] or 3
        self.button_samples = {pin: deque(maxlen=self.config['samples']) for pin in self.listener_map.keys()}
        self.button_states = {pin: False for pin in self.listener_map.keys()}
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: 'running')

        for pin, button in self.listener_map.items():
            msg = 'Setting up listener for GPIO {} mapped to button {}.'.format(pin, button)
            self.logger.info(msg)

            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        for pin, button in self.include_map.items():
            self.logger.info('Configuring include for GPIO {} mapped to button {}.'.format(pin, button))
            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def pressed(self, key, button):
        msg = 'Button {} pressed (pin {}).'.format(button, key)
        self.logger.info(msg)
        self.trigger.pushed(button, self.get_includes())

    def get_includes(self):
        includes = {}

        for key, button in self.include_map.items():
            includes[button] = GPIO.input(key)

        return includes

    def run(self):
        while True:
            for pin, samples in self.button_samples.items():
                samples.append(GPIO.input(pin) == GPIO.LOW)
                if all(samples) and not self.button_states[pin]:
                    self.button_states[pin] = True
                    self.pressed(pin, self.listener_map[pin])
                elif not any(samples) and self.button_states[pin]:
                    self.button_states[pin] = False
                    self.logger.debug('Button pin {} no longer depressed'.format(pin))
            time.sleep(0.05)

if __name__ == "__main__":
    GPIO.setmode(GPIO.BOARD)
    buttons = ButtonService()
    buttons.run()
