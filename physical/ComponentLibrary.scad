
enclosure_inner_length = 80;
enclosure_inner_width = 120;
enclosure_inner_depth = 40;

enclosure_thickness = 3;

cover_thickness = 3;

part = "cover"; // [enclosure:Enclosure, cover:Cover, both:Enclosure and Cover]

print_part();

module print_part() {
	if (part == "enclosure") {
		box2(enclosure_inner_length,enclosure_inner_width,enclosure_inner_depth,enclosure_thickness,enclosure_thickness/2-0.10,cover_thickness);
	} else if (part == "cover") {
		lid2(enclosure_inner_length,enclosure_inner_width,enclosure_inner_depth,enclosure_thickness,enclosure_thickness/2+0.10,cover_thickness);
	} else {
		both();
	}
}

module both() {

box2(enclosure_inner_length,enclosure_inner_width,enclosure_inner_depth,enclosure_thickness,enclosure_thickness/2-0.10,cover_thickness);
lid2(enclosure_inner_length,enclosure_inner_width,enclosure_inner_depth,enclosure_thickness,enclosure_thickness/2+0.10,cover_thickness);

}



//Red/green push button, and multi-color 12v lit toggle both 16.5mm hole
//Fuse Holder is 12.5mm hole

//color("red")
//translate([0, 0, 1])
//linear_extrude(2) {
//    resize([20, 20])
//    import("elec-svgrepo-com.svg", dpi=96);
//}
//color("red")
//translate([22, 0, 1])
//linear_extrude(2) {
//    resize([20, 20])
//    import("emc-svgrepo-com.svg", dpi=96);
//}
//color("red")
//translate([0, 22, 1])
//linear_extrude(2) {
//    resize([20, 20])
//    import("act-svgrepo-com.svg", dpi=96);
//}
//color("red")
//translate([22, 22, 1])
//linear_extrude(2) {
//    resize([20, 20])
//    import("nio-svgrepo-com.svg", dpi=96);
//}

//
//translate([-2, -2, 0]){
//    cube([46, 46, 2]);    
//}

//translate([0, 0, 2])
    //esp32();
    
module Esp32() {
    height = 20;
    h_offset = 44;
    v_offset = 21;
    
    translate([0, 9.5, 0])
    difference() { 
        cube([48, 6, height]);
            
        rotate([90, 90, 0])
        translate([0, 24, -8])
            cylinder(h = 10, d = height - 4);
    }
    
    translate([0, 0, 0])
        cube([4, 25, height]);
    
    translate([44, 0, 0])
        cube([4, 25, height]);
    
    translate([2, 2, height]) Stand_Off();
    translate([2, v_offset + 2, height]) Stand_Off();    
    translate([h_offset + 2, 2, height]) Stand_Off();
    translate([h_offset + 2, v_offset + 2, height]) Stand_Off();
}
    


   // 12vRelay();
//difference (){
//    cube([30, 30, 2]);
//    translate([16, 16, -2])
//        cylinder(h = 40, d = 12.5);
//        //Circular_WS2812_Cutout(true);
//}



//translate([20, 20, 0])
        //Circular_WS2812_Cutout(false);



module Stand_Off() {
    difference() {
        cylinder(2.4, 2, 2, $fn=32);    
        //Inner diameter 1.6mm - targeting m1.7 screws.
        cylinder(2.5, 0.8, 0.8, $fn=32);
    }
}

module Large_Button(thickness) {
    cylinder(thickness + 2, d=16.5, $fn=32);
}

module Banana_Plug(thickness, dia=4) {
    cylinder(thickness + 2, dia, dia, $fn=32);
}

module LED(thickness) {
    cylinder(thickness + 2, d=5.5, $fn=32);
}


module Cable_Tie() {
    difference () {
        cylinder(h=3, r=3, $fn=32);
        cylinder(h=3, r=1.5, $fn=32);       
        translate([-3, -6, -2]) cube(6);
    }    
}

// Generic MOSFET driver module
module IRF520() {
    h_offset = 28.8;
    v_offset = 0;
    
    translate([0, 0, 0]) Stand_Off();
    translate([h_offset, v_offset, 0]) Stand_Off();
}

// Generic Buck Converter Module
module LM2596() {
    h_offset = 29.5;
    v_offset = 17;
    
    translate([0, v_offset, 0]) Stand_Off();
    translate([h_offset, 0, 0]) Stand_Off();
}

// Generic 12v Relay Module
module 12vRelay() {
    h_offset = 45;
    v_offset = 20;
    
    translate([0, 0, 0]) Stand_Off();
    translate([0, v_offset, 0]) Stand_Off();    
    translate([h_offset, 0, 0]) Stand_Off();
    translate([h_offset, v_offset, 0]) Stand_Off();
}




// N.B. Needs to be differenced...
module DC_Jack(shell=3) {
    translate([0, 0, shell/2]) cylinder(10, 6, 6, $fn=32);
    translate([0, 0, 0]) cylinder(10, 4.1, 4.1, $fn=32);
}

// N.B. Needs to be differenced...
module DC_Jack_PreSoldered(shell=3) {
    translate([0, 0, shell/2]) cylinder(10, 7, 7, $fn=32);
    translate([0, 0, 0]) cylinder(10, 5.75, 5.75, $fn=32);
} 


module Circular_WS2812_Cutout(cutout=false) {
    if (cutout) {
        cylinder(9, 12.5, 12.5, $fn=128);
    } else {
        difference () {
            union () {
                translate([-4 + 9, -4 + 9, 0]) cube([8, 8, 2]);
                translate([-4 - 9, -4 - 9, 0]) cube([8, 8, 2]);                
            }
            translate([-2 + 9.5, -2 + 9.5, 0]) cylinder(10, 1, 1, $fn=16);
            translate([2 - 9.5, 2 - 9.5, 0]) cylinder(10, 1, 1, $fn=16);
            
        }
    }
}

module Big_Green_Button(cutout=false) {
    if (cutout) {
        cylinder(22, 12.5, 12.5, $fn=128);
    } else {
        cylinder(11, 18, 18, $fn=128);
    }
}



// Enclosure stuff
module screws(in_x, in_y, in_z, shell) {
	sx = in_x/2 - 4;
	sy = in_y/2 - 4;
	sh = shell + in_z - 12;
	nh = shell + in_z - 4;

    translate([0,0,0]) {
        translate([sx , sy, sh]) cylinder(r=1.4, h = 15, $fn=32);
        translate([sx , -sy, sh ]) cylinder(r=1.4, h = 15, $fn=32);
        translate([-sx , sy, sh ]) cylinder(r=1.4, h = 15, $fn=32);
        translate([-sx , -sy, sh ]) cylinder(r=1.4, h = 15, $fn=32);
    }
}

module bottom(in_x, in_y, in_z, shell) {
	hull() {
   	 	translate([-in_x/2+shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=shell, $fn=32);
		translate([+in_x/2-shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=shell, $fn=32);
		translate([+in_x/2-shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=shell, $fn=32);
		translate([-in_x/2+shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=shell, $fn=32);
	}
}

module sides(in_x, in_y, in_z, shell) {
    translate([0,0,shell])
    difference() {

        hull() {
            translate([-in_x/2+shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=in_z, $fn=32);
            translate([+in_x/2-shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=in_z, $fn=32);
            translate([+in_x/2-shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=in_z, $fn=32);
            translate([-in_x/2+shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=in_z, $fn=32);
        }

        hull() {
            translate([-in_x/2+shell, -in_y/2+shell, 0]) cylinder(r=shell,h=in_z+1, $fn=32);
            translate([+in_x/2-shell, -in_y/2+shell, 0]) cylinder(r=shell,h=in_z+1, $fn=32);
            translate([+in_x/2-shell, in_y/2-shell, 0]) cylinder(r=shell,h=in_z+1, $fn=32);
            translate([-in_x/2+shell, in_y/2-shell, 0]) cylinder(r=shell,h=in_z+1, $fn=32);
        }
    }

    intersection() {
        translate([-in_x/2, -in_y/2, shell]) cube([in_x, in_y, in_z+2]);

        union() {
        translate([-in_x/2 , -in_y/2,shell + in_z -6]) cylinder(r=9, h = 6, $fn=64);
        translate([-in_x/2 , -in_y/2,shell + in_z -10]) cylinder(r1=3, r2=9, h = 4, $fn=64);

        translate([in_x/2 , -in_y/2, shell + in_z -6]) cylinder(r=9, h = 6, $fn=64);
        translate([in_x/2 , -in_y/2, shell + in_z -10]) cylinder(r1=3, r2=9, h = 4, $fn=64);

        translate([in_x/2 , in_y/2,  shell + in_z -6]) cylinder(r=9, h = 6, $fn=64);
        translate([in_x/2 , in_y/2,  shell + in_z -10]) cylinder(r1=3, r2=9, h = 4, $fn=64);

        translate([-in_x/2 , in_y/2, shell + in_z -6]) cylinder(r=9, h = 6, $fn=64);
        translate([-in_x/2 , in_y/2, shell + in_z -10]) cylinder(r1=3, r2=9, h = 4, $fn=64);
        }
    }
}

module lid_top_lip2(in_x, in_y, in_z, shell, top_lip, top_thickness) {

	cxm = -in_x/2 - (shell-top_lip);
	cxp = in_x/2 + (shell-top_lip);
	cym = -in_y/2 - (shell-top_lip);
	cyp = in_y/2 + (shell-top_lip);

	translate([0,0,shell+in_z])

    difference() {

        hull() {
            translate([-in_x/2+shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=top_thickness, $fn=32);
            translate([+in_x/2-shell, -in_y/2+shell, 0]) cylinder(r=shell*2,h=top_thickness, $fn=32);
            translate([+in_x/2-shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=top_thickness, $fn=32);
            translate([-in_x/2+shell, in_y/2-shell, 0]) cylinder(r=shell*2,h=top_thickness, $fn=32);
        }

        
        translate([0, 0, -1]) linear_extrude(height = top_thickness + 2) polygon(points = [
            [cxm+5, cym],
            [cxm, cym+5],
            [cxm, cyp-5],
            [cxm+5, cyp],
            [cxp-5, cyp],
            [cxp, cyp-5],
            [cxp, cym+5],
            [cxp-5, cym]]);
    }
}

module lid2(in_x, in_y, in_z, shell, top_lip, top_thickness) {

	cxm = -in_x/2 - (shell-top_lip);
	cxp = in_x/2 + (shell-top_lip);
	cym = -in_y/2 - (shell-top_lip);
	cyp = in_y/2 + (shell-top_lip);	
    
    union () {
        difference() {
            translate([0, 0, in_z+shell]) linear_extrude(height = top_thickness ) polygon(points = [
                [cxm+5, cym],
                [cxm, cym+5],
                [cxm, cyp-5],
                [cxm+5, cyp],
                [cxp-5, cyp],
                [cxp, cyp-5],
                [cxp, cym+5],
                [cxp-5, cym]]);                    

            screws(in_x, in_y, in_z, shell);                
            translate([-20, 0, in_z + 3]) rotate([0, 0, 0]) Circular_WS2812_Cutout(cutout=true);	

            translate([-20, -35, in_z + 3]) Large_Button(top_thickness); 
            translate([-20, 35, in_z + 3]) Large_Button(top_thickness);             
            translate([20, 35, in_z + 3]) Large_Button(top_thickness);             
            translate([25, -40, in_z + 3]) Large_Button(top_thickness); 
            translate([25, -5, in_z + 3]) Large_Button(top_thickness);             
        }
        
        translate([-20, 0, in_z + 3]) rotate([0, 0, 45]) Circular_WS2812_Cutout(cutout=false);	
        
        color("red")
        translate([12, 35 - (16.5/2) - 5, in_z + 3])
        linear_extrude(4) {
            resize([10, 10])
            rotate([0, 0, 90])
            import("elec-svgrepo-com.svg", dpi=96);
        }
        
        color("green")
        translate([12, 35 + (16.5/2) - 5, in_z + 3])
        linear_extrude(4) {
            resize([10, 10])
            rotate([0, 0, 90])
            import("nio-svgrepo-com.svg", dpi=96);
        }
        
        color("green")
        translate([15, -40 - 5, in_z + 3])
        linear_extrude(4) {
            resize([10, 10])
            rotate([0, 0, 90])
            import("waves-svgrepo-com.svg", dpi=96);
        }
        
        color("red")
        translate([15, -5 - 5, in_z + 3])
        linear_extrude(4) {
            resize([10, 10])
            rotate([0, 0, 90])
            import("emc-svgrepo-com.svg", dpi=96);
        }
    }       
}

module box2(in_x, in_y, in_z, shell, top_lip, top_thickness) {
	bottom(in_x, in_y, in_z, shell);
	
    difference() {
		sides(in_x, in_y, in_z, shell);		
        screws(in_x, in_y, in_z, shell);                    
        translate([0, -in_y/2, 10]) rotate([90, 90, 0]) DC_Jack_PreSoldered();
        translate([-in_y/4, -in_y/2, 10]) rotate([90, 90, 0]) LED(shell);
	}
    
	lid_top_lip2(in_x, in_y, in_z, shell, top_lip, top_thickness);
    
    rotate([0, 0, 90]) translate([-10, -30, shell]) LM2596();
    rotate([0, 0, 90]) translate([-20, 10, shell]) Esp32();
    
    color("red")
    translate([-10, -50, shell]) rotate([90, 0, 0]) Cable_Tie();    
    color("red")
    translate([10, -50, shell]) rotate([90, 0, 0]) Cable_Tie();
    
    color("red")
    translate([-10, -30, shell]) rotate([90, 0, 0]) Cable_Tie();    
    color("red")
    translate([10, -30, shell]) rotate([90, 0, 0]) Cable_Tie();
    color("red")
    translate([-30, -30, shell]) rotate([90, 0, 0]) Cable_Tie();    
    
    color("red")
    translate([-35, 5, shell]) rotate([90, 0, 0]) Cable_Tie();
    color("red")
    translate([-10, 5, shell]) rotate([90, 0, 0]) Cable_Tie();
}


