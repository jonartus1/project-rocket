
//Circular_WS2812_Cutout();

difference() {
    cylinder(3, 14.5, 14.5, $fn=128);
    cylinder(2.6, 12.5, 12.5, $fn=128);
    translate([-2 + 9.5, -2 + 9.5, 0]) cylinder(10, 1, 1, $fn=16);
    translate([2 - 9.5, 2 - 9.5, 0]) cylinder(10, 1, 1, $fn=16);
}


module Circular_WS2812_Cutout(cutout=false) {
    if (cutout) {
        cylinder(9, 12.5, 12.5, $fn=128);
    } else {
        difference () {
            union () {
                translate([-4 + 9, -4 + 9, 0]) cube([8, 8, 2]);
                translate([-4 - 9, -4 - 9, 0]) cube([8, 8, 2]);                
            }
            translate([-2 + 9.5, -2 + 9.5, 0]) cylinder(10, 1, 1, $fn=16);
            translate([2 - 9.5, 2 - 9.5, 0]) cylinder(10, 1, 1, $fn=16);
            
        }
    }
}