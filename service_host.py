import json
import os
import subprocess
import atexit
from time import sleep
import threading
import mqtt_config
import global_config
from heartbeat import Heartbeat


class ServiceHost:
    def __init__(self):
        self.hostname = global_config.get_hostname()
        self.services = global_config.get_service_names()
        
        self.logger = mqtt_config.create_logger(self.hostname.lower(), Hardware=True)
        self.logger.info("Starting service host on {} with version {}".format(global_config.get_hostname(),
                                                                              global_config.config['version']))
        self.sub = mqtt_config.Subscriber('commands/admin', self.message_received)
        self.dir = os.path.dirname(os.path.realpath(__file__))
        self.processes = {}
        self.started = False
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: self.started and 'started' or 'stopped', hardware=True)
        self.check_subprocesses_thread = threading.Thread(target=self.check_subprocesses, args=(), daemon=True)

    def run(self):
        self.start_all_services()
        self.check_subprocesses_thread.start()
        atexit.register(self.at_exit)

    def run_manual(self):
        while True:
            c = input('Button?')
            self.process(c)

    def message_received(self, msg):
        if msg.host not in ['*', global_config.get_hostname()]:
            return

        self.process(msg.operation)
        
    def process(self, command):
        if command == 'start':
            self.start_all_services()
        elif command == 'stop':
            self.stop_all_services()
        elif command == 'list':
            print('Active services:')
            for proc in self.processes.values():
                print(proc.pid)
        elif command == 'check':
            for proc in self.processes.values():
                out, err = proc.communicate()
                print(proc.returncode)
        elif command == 'restart':
            print("Restarting...")
            self.stop_all_services()
            self.start_all_services()
        elif command == 'update':
            print("Updating...")
            self.stop_all_services()
            self.update()
            self.start_all_services()

    def start_all_services(self):
        if self.started:
            return

        self.logger.info('Starting services...')

        for service in self.services:
            self.start_service(service)
        self.started = True
        self.logger.info('Started {} services.'.format(len(self.processes)))

    def start_service(self, service):
        command = "{}/{}.py".format(self.dir, service)        
        python_exec = global_config.get_config_section("python_exec")
        self.logger.debug('Opening process with command: ' + python_exec + ' ' + command)

        proc = subprocess.Popen([python_exec, command])
        self.logger.info('Started {} with pid {}.'.format(service, proc.pid))
        self.processes[service] = proc

        config = global_config.get_service_config(service)

        if 'wait_after_start' in config:
            sleep(config['wait_after_start'])

    def at_exit(self):
        self.logger.debug('Force exit detected, stopping all services...')
        self.stop_all_services()

    def stop_all_services(self):
        self.logger.info('Stopping services...')
        # Mark stopped immediately to prevent auto restarts
        self.started = False

        for service, proc in self.processes.items():
            self.logger.info('Stopping {} with pid {}.'.format(service, proc.pid))
            proc.terminate()

        for service, proc in self.processes.items():
            try:
                proc.wait(timeout=5)
            except subprocess.TimeoutExpired:
                self.logger.info('Killing {} with pid {}.'.format(service, proc.pid))
                proc.kill()

        self.processes = {}

    def update(self):
        os.system('pwd')        
        if not self.wait_for_process('git', 'pull'):
            self.logger.warn('git pull failed!')

    def wait_for_process(self, *args, timeout=30):
        self.logger.debug("Running process %s", ' '.join(args))
        proc = subprocess.Popen(args)

        try:
            result = proc.wait(timeout=timeout)
        except subprocess.TimeoutExpired:
            self.logger.exception('Process {} has not completed after {}s.'.format(args, timeout))
            proc.terminate()
            return False
        return result == 0

        if proc.wait(timeout=5) is None:
            self.logger.info('Killing {} with pid {}.'.format(args, proc.pid))
            proc.kill()
        return False
      
    def check_subprocesses(self):
        while True:
          if self.started:
            for service, proc in self.processes.items():
                result = proc.poll()
                if result is not None:
                    self.logger.warn('Service {} exited with status {}'.format(service, result))
                    self.start_service(service)
          sleep(1)
        
if __name__ == "__main__":
    host = ServiceHost()
    host.run()    
    
    input("Press any key to exit...\n")
