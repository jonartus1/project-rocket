from enum import IntEnum

class PhysicalInputs(IntEnum):
    R_Pot = 10,
    G_Pot = 11,
    B_Pot = 12,
    M5_Auth_A = 20,
    M5_Auth_B = 21,
    M5_Auth_C = 22,
    