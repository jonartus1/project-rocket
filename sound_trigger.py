from enum import Enum
import mqtt_config
import json


class Channels(Enum):
    main = 1
    crawlspace = 2
    atmosphere = 3
    fm = 4


class Pan(Enum):
    both = 1
    left = 2
    right = 3


class Operations(Enum):
    play = 1
    pause = 2
    stop = 3
    speak = 4
    play_music = 11
    stop_music = 12


class SoundTrigger:
    def __init__(self):
        self.mqtt = mqtt_config.create_mqtt()
        self.mqtt_prefix = "commands/service/sound."

    def play(self, channel, filename, volume=1.0, pan=Pan.both, loops=0):
        req = {'operation': Operations.play.name, 'filename': filename, 'volume': volume, 'pan': pan.name, 'loops': loops}
        self.mqtt.publish(self.mqtt_prefix + channel.name, json.dumps(req))

    def stop(self, channel):
        req = {'operation': Operations.stop.name}
        self.mqtt.publish(self.mqtt_prefix + channel.name, json.dumps(req))

    def speak(self, channel, text, volume=1.0, pan=Pan.both):
        req = {'operation': Operations.speak.name, 'text': text, 'volume': volume, 'pan': pan.name}
        self.mqtt.publish(self.mqtt_prefix + channel.name, json.dumps(req))

    def play_music(self, channel, filename, volume=1.0):
        req = {'operation': Operations.play_music.name, 'filename': filename, 'volume': volume}
        self.mqtt.publish(self.mqtt_prefix + channel.name, json.dumps(req))

    def stop_music(self, channel):
        req = {'operation': Operations.stop_music.name}
        self.mqtt.publish(self.mqtt_prefix + channel.name, json.dumps(req))
