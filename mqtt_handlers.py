import logging
import json
from datetime import datetime

default_log_format = '%(asctime)-15s %(name)-20s %(levelname)8s  %(message)s'

class MqttFormatter(logging.Formatter):
    def format(self, record):
        """
        JSON-encode a record for serializing through MQTT.

        Convert date to iso format, and stringify any exceptions.
        """
        data = record.__dict__.copy()
        
        # serialize the datetime date as utc string
        data['time'] = datetime.fromtimestamp(data['created']).isoformat()
        data['message'] = data['msg'] % data['args']

        # stringify exception data
        if 'exc_info' in data and data['exc_info'] is not None:
            data['exc_info'] = self.formatException(data['exc_info'])

        return json.dumps(data)

class MqttOutputFormatter(logging.Formatter):
    '''
    Standard log formatter but handles the log messages coming from the 
    `MqttFormatter` above.
    '''
    def __init__(self, format=default_log_format):
        super(MqttOutputFormatter, self).__init__(format)
    
    def format(self, data):
        exc_info = data['exc_info']
        data['exc_info'] = None
        record = logging.makeLogRecord(data)
        output = super(MqttOutputFormatter, self).format(record)
        if exc_info is not None:
            output += '\n' + exc_info
        return output

class MqttHandler(logging.Handler):
    """
    Publish messages to MQTT channel.
    """

    def __init__(self, channel, mqtt_client, level=logging.DEBUG):
        """
        Create a new logger for the given channel and mqtt_client.
        """
        logging.Handler.__init__(self, level)
        self.channel = channel
        self.mqtt_client = mqtt_client
        self.formatter = MqttFormatter()

    def emit(self, record):
        """
        Publish record to mqtt logging channel
        """
        self.mqtt_client.publish(self.channel, self.format(record))
