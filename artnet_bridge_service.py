import threading
import mqtt_config
import time
import sys
from threading import Thread

from threading import Timer
from socket import (socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR)
from struct import pack, unpack
from global_config import get_config_section

class ArtnetPacket:
    ARTNET_HEADER = b'Art-Net\x00'

    def __init__(self):
        self.op_code = None
        self.ver = None
        self.sequence = None
        self.physical = None
        self.universe = None
        self.length = None
        self.data = None

    def __str__(self):
        return (f"ArtNet package:\n - op_code: {self.op_code}\n - version: {self.ver}\n - "
                f"sequence: {self.sequence}\n - physical: {self.physical}\n - universe: {self.universe}\n - "
                f"length: {self.length}\n - data : {self.data}")

    def unpack_raw_artnet_packet(raw_data):
        if unpack('!8s', raw_data[:8])[0] != ArtnetPacket.ARTNET_HEADER:
            print("Received a non Art-Net packet")
            return None

        if (len(raw_data) < 18):
            print("Received a short package: ")
            return None

        packet = ArtnetPacket()
        (packet.op_code, packet.ver, packet.sequence, packet.physical, packet.universe, packet.length) = unpack('!HHBBHH', raw_data[8:18])
        packet.data = unpack('{0}s'.format(int(packet.length)), raw_data[18:18+int(packet.length)])[0]

        return packet


class Fixture:
    def __init__(self, name, start_index, channels, publish_callback):
        self.name = name
        self.start_index = start_index
        self.channels = channels
        self.channel_data = [0 for i in range(channels)]
        self.publish = publish_callback
        self.changed = False

    def handle_frame(self, dmx):        
        # If any data have changed, record new values and mark changed
        for i in range(self.channels):
            if self.channel_data[i] != dmx[self.start_index + i]:
                self.channel_data[i] = dmx[self.start_index + i]
                self.changed = True
        
    def update_if_needed(self):
        if self.changed:
            self.broadcast_updates()
            self.changed = False

    def broadcast_updates(self):
        color_string = ''

        # Format as a hex string of one 2-Byte word per channel.
        for i in range(self.channels):
            color_string = color_string + f'{self.channel_data[i]:02x}'

        self.publish(f'tasmota/cmnd/{self.name}/Color', color_string)


class ArtnetBridgeService:
    def __init__(self):
        self.logger = mqtt_config.create_logger("artnet_bridge", "master")
        self.mqtt = mqtt_config.create_mqtt()
        self.config = get_config_section('artnet')        
        self.fixtures = []
        self.update_thread = threading.Thread(target=self.update_fixtures)

        for f in self.config['fixtures']:
            fixture = Fixture(f['name'], f['start_index'], f['channels'], lambda t, d: self.mqtt.publish(t, d))
            self.fixtures.append(fixture)
        
    def update_fixtures(self):
        while True:        
            for f in self.fixtures:
                f.update_if_needed()
            
            time.sleep(0.2) # 5FPS is generall enough and avoids MQTT spam!
        

    def run(self):
        ip = self.config['listen_ip']

        # Create a socket for Datagrams (UDP), and allow reuse of the socket (we're listen-only, so should be fine)
        sock = socket(AF_INET, SOCK_DGRAM)
        sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sock.bind((ip, 6454)) # ArtNet is always on 6454 AFAIK
    
        self.update_thread.start()

        while True:
            try:
                data, addr = sock.recvfrom(1024)
                packet = ArtnetPacket.unpack_raw_artnet_packet(data)

                if packet != None:
                    for fixture in self.fixtures:
                        fixture.handle_frame(packet.data)

            except KeyboardInterrupt:
                sock.close()
                sys.exit()


if __name__ == "__main__":
    lighting = ArtnetBridgeService()
    lighting.run()
    
    while True:
        time.sleep(60)
