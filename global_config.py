import socket
from lighting_trigger import LightingScene, LightingChase
from physical_inputs import PhysicalInputs

# Note - don't access this directly as you'll skip the handy overrides...
config = {
    'host_overrides': {
        'jon-ubuntu': {
            'mqtt': {'host': '127.0.0.1'},
        },
        'CRANE': {
            'mqtt': {'host': '192.168.57.10', 'username': 'rocket', 'password': 'mttKsC8vgpYPHH' },
            'python_exec': 'python'
        },
        'Woodchuck': {
            'mqtt': {'host': '127.0.0.1', 'username': 'rocket', 'password': 'mttKsC8vgpYPHH' },
            'python_exec': 'python'
        },
        # 'Woodchuck': {
        #     'mqtt': {'host': '192.168.8.2' },
        #     'python_exec': 'python'
        # }
    },
    'python_exec': 'python',
    'global_namespace': 'rocket',
    'version': '0.9',    
    'mqtt': {'host': '192.168.8.10'},
    'qlc': {'socket': 'ws://localhost:9999/qlcplusWS'},
    'wled': {'state_url': 'http://192.168.8.20/json/state'},
    'mqtt-lighting': {'topic': 'ship/main_lights/set'},
    'artnet': { 
        'listen_ip': '127.0.0.1', # Assume that we'll normally run this on the same host as QLC-Plus
        'fixtures': [
        { 'name': 'Tas-GU10-01', 'start_index': 0, 'channels': 5 },
        { 'name': 'Tas-GU10-02', 'start_index': 5, 'channels': 5 },
        { 'name': 'Tas-GU10-03', 'start_index': 10, 'channels': 5 },
        { 'name': 'Tas-GU10-04', 'start_index': 15, 'channels': 5 },
        { 'name': 'Tas-GU10-05', 'start_index': 20, 'channels': 5 },
        { 'name': 'Tas-GU10-06', 'start_index': 25, 'channels': 5 },
        { 'name': 'Tas-GU10-07', 'start_index': 30, 'channels': 5 },
        { 'name': 'Tas-GU10-08', 'start_index': 35, 'channels': 5 }
    ]},
    'services': {        
        'PI-01': {
            'sound_service': {'channel': 'main'},
            'qlc_service': {'order': 1, 'wait_after_start': 10},
            # 'artnet_bridge_service': {'order': 2},
            'lighting_service': {'order': 3},
        },
        'Woodchuck': {
            'rocket_ship': { },
            # 'lighting_service': {'order': 3}
        },
        'PI-03': {
            'gpio_listener': {
                'buttons': {
                    
                },
                'adcs': {

                }
            },
        }                
    },
    'game_variables': {
        'known_uids': {
            '884d01d41': { 'name': 'Captain Jon' }
        }        
    },
    'lighting_presets': {
        'scenes': {
            LightingScene.CurtainWarmer: {
                'WLED': 2,
                'QLC': 'EnginesStandby'                
            },
            LightingScene.NormalRunning: {
                'WLED': 3,
                'QLC': 'EnginesMain',
                'MQTT': { 'r': 64, 'g': 128, 'b': 192 }
            }
        },
        'chases': {
            LightingChase.LaserShot: {
                'QLC': 'LaserShot'
            }
        }
    }
}


def get_hostname():
    return socket.gethostname()


def get_service_names():
    print('Getting services for {}.'.format(get_hostname()))
    services = list(config['services'][get_hostname()].keys())
    print(services)
    services.sort(key=lambda s: get_service_config(s)['order'] if 'order' in get_service_config(s) else 100)
    print(services)
    return services


def get_service_config(service_name):
    return config['services'][get_hostname()][service_name]

def get_device_config(device):
    return config['services'][device]

def get_config_section(section_name):
    hostname = get_hostname()

    if hostname in config['host_overrides']:
        if section_name in config['host_overrides'][hostname]:
            return config['host_overrides'][hostname][section_name]

    return config[section_name]

# qq make overrides work here.
def get_all_config():
    return config

def get_variable(name):
    return config['game_variables'][name]
