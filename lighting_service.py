import itertools
from os import getcwdb
import mqtt_config
import time
from lighting_trigger import LightingOperations, LightingScene
from websocket import create_connection
from global_config import get_config_section
from threading import Timer
from requests import post
from json import dumps

def group(lst, n):
    return zip(*[itertools.islice(lst, i, None, n) for i in range(n)])


def format_message(msg):
    return f"Setting scene {msg.scene} to state {msg.operation}"

class MqttInterface:
    def __init__(self):
        self.logger = mqtt_config.create_logger("mqtt-interface")
        self.client = mqtt_config.create_mqtt()

        mqtt_lights_config = get_config_section('mqtt-lighting')
        self.topic = mqtt_lights_config['topic']

    def run(self):
        pass

    def set_scene(self, scene, intensity=255):
        self.client.publish(self.topic, dumps(scene))

    def play_chase(self, msg):
        pass

class WledInterface:
    def __init__(self):
        self.logger = mqtt_config.create_logger("wled-interface")
        
        led_config = get_config_section('wled')
        self.wled = led_config['state_url']

    def run(self):
        pass

    def set_scene(self, scene, intensity=255):
        post(self.wled, json={ 'ps': scene })

    def play_chase(self, msg):
        pass

class QlcInterface:
    def __init__(self):
        self.logger = mqtt_config.create_logger("qlc-interface")
    
    def run(self):
        qlc_config = get_config_section('qlc')
        
        self.qlc = create_connection(qlc_config['socket'])
        self.qlc.send('QLC+IO|OUTPUT|0|DMX USB|0')

        self.widgets = self.load_widget_lookup()
        print(self.widgets)

    def load_widget_lookup(self):
        self.qlc.send('QLC+API|getWidgetsList')
        result = self.qlc.recv()
        parts = result.split('|')
        pairs = group(parts[2:], 2)
        channel_map = {}

        for channel, name in pairs:
            print("Found Widget: " + name)
            channel_map[name] = channel

        return channel_map

    def set_scene(self, scene, intensity):
        try:
            channel_id = self.widgets[scene]
            self.qlc.send('{}|{}'.format(channel_id, intensity))            
        except:
            self.logger.exception('Error setting scene {} to state on.'.format(scene))

    def play_chase(self, msg):
        try:
            channel_id = self.widgets[msg.scene]
            self.qlc.send('{}|{}'.format(channel_id, 0))
        except:
            self.logger.exception('Error setting scene {} to state off.'.format(msg.scene))

class LightingService:
    def __init__(self):
        self.mqtt_topic = "commands/service/lighting"
        self.logger = mqtt_config.create_logger('lighting')
        self.interfaces = {
            'QLC': QlcInterface(),
            'WLED': WledInterface(),
            'MQTT': MqttInterface() 
        }

        self.scenes = get_config_section('lighting_presets')['scenes']
        self.chases = get_config_section('lighting_presets')['chases']
        
    def run(self):        
        for interface in self.interfaces.values():
            interface.run()

        self.subscription = mqtt_config.Subscriber(self.mqtt_topic, self.message_received)

    def message_received(self, msg):
        print (msg.get_raw())

        if msg.operation == LightingOperations.scene.name:
            self.set_scene(msg)
        elif msg.operation == LightingOperations.chase.name:
            self.play_chase(msg)
        
    def set_scene(self, msg):
        new_scene = self.scenes[LightingScene[msg.scene]]

        for interface, if_scene in new_scene.items():
            self.interfaces[interface].set_scene(if_scene, msg.intensity)
        

    def play_chase(self, msg):
        pass

if __name__ == "__main__":
    lighting = LightingService()
    lighting.run()
    
    while True:
        time.sleep(60)
