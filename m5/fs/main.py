from m5stack import *
from m5ui import *
from uiflow import *
from m5mqtt import M5mqtt

import wifiCfg
import json
import ntptime
import time
import unit


rfid_scanner = unit.get(unit.RFID, unit.PORTA)
pir_sensor = unit.get(unit.PIR, unit.PORTB)

state = {
    'host_overrides': { }
}


class rfid_reader:
  def __init__(self, mqtt):
    self.mqtt = mqtt
    self.reader = unit.get(unit.RFID, unit.PORTA)
    self.current_value = ''
    self.last_read_ticks = 0
    
  def loop(self):
    if (time.ticks_ms() - self.last_read_ticks) < 500:
      return

    self.last_read_ticks = time.ticks_ms()

    if self.reader.isCardOn():
      new_value = self.reader.readUid()
      
      if new_value != self.current_value:
        self.mqtt.publish('event/rfid/card_scanned', json.dumps({'uid': new_value}))
        self.current_value = new_value        
    else:
      self.current_value =''

class heartbeat:
  def __init__(self, mqtt, ntp):
    self.mqtt = mqtt
    self.ntp = ntp
    self.last_read_ticks = 0

  def loop(self):
    if (time.ticks_ms() - self.last_read_ticks) < 5000:
      return

    self.last_read_ticks = time.ticks_ms()

    self.mqtt.publish('heartbeat/M5_RFID', json.dumps({'uptime': time.ticks_ms(), 'timestamp': ntp.getTimestamp()}), 0)


class lights:
  def __init__(self, mqtt):
    self.mqtt = mqtt
    self.index = 0
    self.max = 10
    self.color = 0xff0000
    self.last_ticks = 0
    self.mode = 'chase'

    rgb.setColorAll(0x000000)
    rgb.setBrightness(255)
    
  def loop(self):
    if (time.ticks_ms() - self.last_ticks) < 200:
      return

    if self.mode == 'chase':
      self.chase()
    if self.mode == 'solid':
      self.solid()

  def chase(self):
    self.last_ticks = time.ticks_ms()

    rgb.setColor(self.index, 0x000000)

    self.index += 1

    if self.index > self.max:
      self.index = 1

    rgb.setColor(self.index, self.color)

  def solid(self):
    rgb.setColorAll(self.color)
    rgb.setBrightness(255)
    
lcd.clear()
setScreenColor(0x000000)
title0 = M5Title(title="Rocket Ship v1.0 Initialising...", x=3, fgcolor=0xFFFFFF, bgcolor=0x0000FF)
title0.show()


wifiCfg.doConnect('Rocket', 'Rocketsh1p')

while not (wifiCfg.wlan_sta.isconnected()):
  title0.setTitle(str('Rocket Ship v1.0 Connecting Wifi...'))

title0.setTitle(str('Rocket Ship v1.0 Getting Time...'))
ntp = ntptime.client(host='pool.ntp.org', timezone=0)

title0.setTitle(str('Rocket Ship v1.0 Connecting MQTT...'))
m5mqtt = M5mqtt('M5_RFID', '192.168.8.2', 1883, '', '', 300)
m5mqtt.start()
m5mqtt.publish(str('device/M5_RFID'), json.dumps({'status': 'Connected', 'timestamp': ntp.getTimestamp()}), 0)
m5mqtt.set_last_will(str('device/M5_RFID'),str('I Die!'))

title0.hide()


image0 = M5Img(0, 0, "res/default.jpg", True)

lcd.font(lcd.FONT_DejaVu18)
lcd.setTextColor(lcd.GREEN)
lcd.text(0, 60, "Foo.")


def on_a_pressed():
  m5mqtt.publish('event/physical_input', json.dumps({'type':'button_pushed', 'input':20}), 0)
  lcd.text(0, 100, "A")

btnA.wasPressed(on_a_pressed)

def on_b_pressed():
  m5mqtt.publish('event/physical_input', json.dumps({'type':'button_pushed', 'input':21}), 0)
  lcd.text(0, 100, "B")

btnB.wasPressed(on_b_pressed)

def on_c_pressed():
  m5mqtt.publish('event/physical_input', json.dumps({'type':'button_pushed', 'input':22}), 0)
  lcd.text(0, 100, "C")

btnC.wasPressed(on_c_pressed)


loop_targets = [
  heartbeat(m5mqtt, ntp),
  rfid_reader(m5mqtt),
  lights(m5mqtt)
]


while True:
  for item in loop_targets:
    item.loop()

  wait_ms(10)