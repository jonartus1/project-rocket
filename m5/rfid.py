from m5stack import *
from m5ui import *
from uiflow import *
from m5mqtt import M5mqtt
import json

import time
import unit


setScreenColor(0x000000)
rfid_0 = unit.get(unit.RFID, unit.PORTA)
pir_0 = unit.get(unit.PIR, unit.PORTB)


payload = None
r = None
activated = None
light_getting_brighter = None
light_intensity = None
g = None
motion_detected = None
b = None



circle0 = M5Circle(95, 90, 15, 0xFFFFFF, 0xFFFFFF)
circle1 = M5Circle(220, 90, 15, 0xFFFFFF, 0xFFFFFF)
RFIDData = M5TextBox(12, 212, "Text", lcd.FONT_Default, 0xFFFFFF, rotate=0)

from numbers import Number


# Describe this function...
def awaiting_login():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  rgb.setColorAll(0xff0000)
  for count in range(10):
    pulse_lights()
  if rfid_0.isCardOn():
    RFIDData.setText(str(rfid_0.readUid()))
    m5mqtt.publish(str('event/rfid/card_scanned'), str((json.dumps(({'uid':(rfid_0.readUid())})))), 0)
  else:
    RFIDData.setText('Awaiting Card')
  wait_ms(10)

# Describe this function...
def pulse_lights():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  if light_getting_brighter:
    light_intensity = (light_intensity if isinstance(light_intensity, Number) else 0) + 3
  else:
    light_intensity = (light_intensity if isinstance(light_intensity, Number) else 0) + -3
  rgb.setBrightness(light_intensity)
  if light_intensity >= 255:
    light_getting_brighter = False
  if light_intensity <= 0:
    light_getting_brighter = True

# Describe this function...
def logged_in():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  if btnA.isPressed():
    activated = False
  rgb.setBrightness(255)
  rgb.setColorAll((r << 16) | (g << 8) | b)
  wait_ms(10)


def fun_ship_main_lights_set_(topic_data):
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  payload = json.loads(topic_data)
  r = payload['r']
  g = payload['g']
  b = payload['b']
  pass

def fun_event_state_changed_(topic_data):
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  if ((json.loads(topic_data))['new_state']) == 'activated':
    activated = True
    speaker.sing(523, 1/2)
    speaker.sing(659, 1/2)
    speaker.sing(784, 1)
  else:
    activated = False
  pass

def buttonA_wasPressed():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  m5mqtt.publish(str('event/physical_input'), str((json.dumps(({'type':'button_pushed','input':20})))), 0)
  pass
btnA.wasPressed(buttonA_wasPressed)

def buttonB_wasPressed():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  m5mqtt.publish(str('event/physical_input'), str((json.dumps(({'type':'button_pushed','input':21})))), 0)
  pass
btnB.wasPressed(buttonB_wasPressed)

def buttonC_wasPressed():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  m5mqtt.publish(str('event/physical_input'), str((json.dumps(({'type':'button_pushed','input':22})))), 0)
  pass
btnC.wasPressed(buttonC_wasPressed)

@timerSch.event('pir_check')
def tpir_check():
  global payload, r, activated, light_getting_brighter, light_intensity, g, motion_detected, b
  if (pir_0.state) == 1:
    if not motion_detected:
      motion_detected = True
      m5mqtt.publish(str('event/motion_detection_changed'), str((json.dumps(({'source':'m5auth_pir','current_state':True})))), 0)
  else:
    if motion_detected:
      motion_detected = False
      m5mqtt.publish(str('event/motion_detection_changed'), str((json.dumps(({'source':'m5auth_pir','current_state':False})))), 0)
  pass


m5mqtt = M5mqtt('M5_RFID', '192.168.57.10', 1883, '', '', 300)
m5mqtt.subscribe(str('ship/main_lights/set'), fun_ship_main_lights_set_)
m5mqtt.subscribe(str('event/state_changed'), fun_event_state_changed_)
m5mqtt.start()
m5mqtt.publish(str('device/M5_RFID'), str('Connected'), 0)
m5mqtt.set_last_will(str('device/M5_RFID'),str('I Die!'))
motion_detected = False
light_intensity = 0
light_getting_brighter = True
activated = False
r = 0
g = 0
b = 0
timerSch.run('pir_check', 500, 0x00)
while True:
  if activated:
    logged_in()
  else:
    awaiting_login()
  wait_ms(2)
