// 20:09:56 CMD: Group 0, Index 1, Command "COLOR", Data "00FF000000"
// 20:09:56 MQT: tasmota/stat/tasmota-GU10-01/RESULT = {"POWER":"ON","Dimmer":100,"Color":"00FF000000","HSBColor":"120,100,100","White":0,"CT":365,"Channel":[0,100,0,0,0]}
// 20:09:57 CFG: Saved to flash at F9, Count 235, Bytes 4096

#include <M5Stack.h>
#include <WiFi.h>
#include <WiFiUdp.h>

#define MY_SSID "Syzygy Corp 2.4G"
#define MY_PWD "Glasonbury123"
#define LISTEN_PORT 6454

WiFiUDP Udp;

void RecieveUdp()
{
  byte packet[18 + 512]; 

  //test if a packet has been recieved
  if (Udp.parsePacket() > 0)
  {
    //read-in packet and get length
    int len = Udp.read(packet, 18 + 512);

    //discard unread bytes
    Udp.flush();

    //test for empty packet
    if(len < 1)
      return;

    //test for Art-Net DMX packet
    //(packet[14] & packet[15] are the low and high bytes for universe)
    if(packet[9] == 0x50)
    {
      int dmx = 18;
      M5.Lcd.setCursor(10, 10);
      
      //copy dmx data to leds
      for(int n = 0; n < 16; n++) {
        M5.Lcd.print(" ");     
        M5.Lcd.print(packet[dmx + n]);     
      }
    }
  }
}

void setup() {
  M5.begin(); //Init M5Core.
  M5.Power.begin(); //Init Power module.
  
  M5.Lcd.fillScreen(BLACK);  
  M5.Lcd.setCursor(10, 10); 
  M5.Lcd.setTextColor(WHITE);
  M5.Lcd.setTextSize(2);  
  M5.Lcd.printf("Connecting to Wifi!");  

  WiFi.begin(MY_SSID, MY_PWD);
  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.clear(BLACK);
    delay(100);
    M5.Lcd.print(".");
    delay(100);
    M5.Lcd.print(".");
    delay(100);
    M5.Lcd.print(".");
    delay(100);
  }

  M5.Lcd.clear(BLACK);
  M5.Lcd.print("Connected! IP: ");  
  M5.Lcd.print(WiFi.localIP());  

  Udp.begin(LISTEN_PORT);
}

void loop(){
  M5.update();
  RecieveUdp();
  delay(50);
}