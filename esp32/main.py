from mqtt_as import MQTTClient
from mqtt_local import wifi_led, blue_led, config
from machine import Pin
from neopixel import NeoPixel
import uasyncio as asyncio
import gc
import json


# Subscription callback
def sub_cb(topic, msg, retained):
    print(f'Topic: "{topic.decode()}" Message: "{msg.decode()}" Retained: {retained}')

# Demonstrate scheduler is operational.
async def heartbeat(config, client):
    s = True
    c = 20
    device_id = config['device_id']
    topic = f'admin/heartbeat/device/{device_id}'

    while True:
        await asyncio.sleep_ms(500)
        blue_led(s)
        s = not s
        c += 1

        if c >= 20:
            await client.publish(topic, f"RAM free {gc.mem_free()} alloc {gc.mem_alloc()}")
            c = 0

class Button:
    def __init__(self, name, gpio):        
        self.name = name
        self.gpio = gpio
        self.pin = Pin(gpio, Pin.IN, Pin.PULL_UP)
        self.last_state = self.state()
        self.topic = f'action/button/{name}'
        self.samples = 0
        print(f"Initial State [{self.last_state}]")
    
    def state(self):
        return self.pin.value()

    def changed(self):
        new_state = self.state()

        if new_state != self.last_state:
            self.samples += 1            
        else:
            self.samples = 0

        # Trigger once after the new state has been present for 5 cycles to debounce things.
        if self.samples == 3:
            print(f"Button {self.name} state changed to [{new_state}]")
            self.last_state = new_state
            return True

        return False


class ToggleButton(Button):
    def __init__(self, name, gpio):
        print(f"Creating toggle button {name} on GPIO{gpio}.")
        super().__init__(name, gpio)

class MomentaryButton(Button):
    def __init__(self, name, gpio):
        print(f"Creating momentary button {name} on GPIO{gpio}.")
        super().__init__(name, gpio)
        self.payload = {
            "event": "pressed"
        }

    def get_update_payload(self):
        if self.last_state:
            self.payload['event'] = "released"
        else:
            self.payload['event'] = "pressed"

        return json.dumps(self.payload)


async def buttons(config, client):
    buttons = [
        MomentaryButton("Button 1", 39),
        MomentaryButton("Button 2", 38),
        MomentaryButton("Button 3", 37)
    ]

    print("Init buttons coroutine...")

    while True:
        for button in buttons:
            if button.changed():
                await client.publish(button.topic, button.get_update_payload())
        
        await asyncio.sleep_ms(20)


async def colours(config, client):    
    try:        
        pin = Pin(15, Pin.OUT)   # set GPIO0 to output to drive NeoPixels
        np = NeoPixel(pin, 10)   # create NeoPixel driver on GPIO0 for 8 pixels
        np[0] = (255, 0, 0) # set the first pixel to white
        np.write()              # write data to all pixels

        print('Running colours...')
        prev = 9

        while True:
            for i in range(10):                
                np[prev] = (0, 0, 0)
                np[i] = (0, 255, 0) 
                np.write()  

                prev = i
                await asyncio.sleep_ms(100)
    except OSError:
        print('Error in Colours co-routine...')
        return

async def wifi_han(state):
    wifi_led(not state)
    print('Wifi is ', 'up' if state else 'down')
    await asyncio.sleep(1)

# If you connect with clean_session True, must re-subscribe (MQTT spec 3.1.2.4)
async def conn_han(client):    
    await client.subscribe('foo_topic', 1)

async def main(client):
    try:
        await client.connect()
    except OSError:
        print('Connection failed.')
        return
    n = 0
    while True:
        await asyncio.sleep(5)
        print('publish', n)
        # If WiFi is down the following will pause for the duration.
        await client.publish('result', '{} {}'.format(n, client.REPUB_COUNT), qos = 1)
        n += 1

# Define configuration
config['subs_cb'] = sub_cb
config['wifi_coro'] = wifi_han
config['connect_coro'] = conn_han
config['clean'] = True

# Set up client
MQTTClient.DEBUG = True  # Optional
client = MQTTClient(config)

asyncio.create_task(heartbeat(config, client))
asyncio.create_task(buttons(config, client))
asyncio.create_task(colours(config, client))

try:
    asyncio.run(main(client))
finally:
    client.close()  # Prevent LmacRxBlk:1 errors
    asyncio.new_event_loop()
