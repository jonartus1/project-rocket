import { defineStore } from 'pinia'

const mqtt = require('mqtt')
const client  = mqtt.connect('ws://localhost:9001')

client.on('connect', function () {
    client.subscribe('presence', function (err) {
      if (!err) {
        client.publish('presence', 'Hello mqtt')
      }
    })
  })
  
client.on('message', function (topic, message) {
    console.log(message.toString())
    client.end()
})

export const mqttStore = defineStore('mqtt', {
    state: () => ({ count: 0, name: 'Eduardo' }),
    getters: {
      doubleCount: (state) => state.count * 2,
    },
    actions: {
      publish(topic, message) {
        client.publish(topic, message);
      },
    },
  })
