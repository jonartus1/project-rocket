import time
import json
import mqtt_config
import global_config
from heartbeat import Heartbeat

class ConfigService:
    def __init__(self):
        self.logger = mqtt_config.create_logger("service", "config")
        self.logger.info(f"Starting qlc service on {global_config.get_hostname()} with version {global_config.config['version']}")
                                                                             
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: 'running', singleton=True)

    def run(self):
        self.mqtt = mqtt_config.create_mqtt()
        self.subscription = mqtt_config.Subscriber('admin/config/request', self.message_received)
        self.logger.info('Started config service.')
        
    def message_received(self, msg):
        if hasattr(msg, "device_id"):
            print("Received request - device id: " + msg.device_id)
            config = global_config.get_device_config(msg.device_id)
            self.mqtt.publish(f'admin/config/{msg.device_id}/', json.dumps(config))
        else:
            print("Received request for full config.")
            config = global_config.get_all_config()
            self.mqtt.publish('admin/config', json.dumps(config))



if __name__ == "__main__":
    config = ConfigService()
    config.run()

    while True:
        time.sleep(60)

