import json
import threading
import traceback
from time import sleep
from datetime import datetime
from mqtt_config import create_mqtt, get_hostname
from global_config import get_variable


class Heartbeat:
    period = 5.0

    def __init__(self, service, get_state=lambda: 'None', hardware=False):
        self.service = service
        self.hardware = hardware # For the root service host, which broadcasts in a different namespace
        self.hostname = get_hostname()

        if hardware:
            self.channel = f'hardware/{self.hostname}'
        else:
            self.channel = f'service/{self.service}'
        
        print('Creating heartbeat for ' + self.channel)

        self.mqtt = create_mqtt()
        self.get_state = get_state
        self.run_thread = threading.Thread(target=self.run, args=())
        self.run_thread.daemon = True
        self.run_thread.start()

    def run(self):
        while True:
            try:
                self.send()
                sleep(Heartbeat.period)
            except:
                traceback.print_exc()

    def send(self):
        self.mqtt.publish(self.channel, json.dumps(self.generate_heartbeat()))

    def generate_heartbeat(self):
        return {
            'timestamp': datetime.now().timestamp(),
            'state': self.get_state(),
            'host': self.hostname,
            'service': self.service
        }
