import mqtt_config
import json
from enum import Enum


# Note: These need to match the WLED scene IDs
class LightingScene(Enum):
    Blackout = 1,  
    CurtainWarmer = 2,
    NormalRunning = 3

class LightingChase(Enum):
    LaserShot = 1,  

class LightingOperations(Enum):
    scene = 1
    chase = 2


class LightingTrigger:
    def __init__(self):
        self.mqtt_topic = "commands/service/lighting"
        self.mqtt = mqtt_config.create_mqtt()

    def scene(self, scene, intensity = 255):
        req = {'operation': LightingOperations.scene.name, 'scene': LightingTrigger.get_scene_name(scene), 'intensity': intensity}
        self.mqtt.publish(self.mqtt_topic, json.dumps(req))

    def chase(self, chase):
        req = {'operation': LightingOperations.chase.name, 'chase': LightingTrigger.get_scene_name(chase)}
        self.mqtt.publish(self.mqtt_topic, json.dumps(req))

    @staticmethod
    def get_scene_name(scene):
        if isinstance(scene, LightingScene):
            return scene.name

        return scene
