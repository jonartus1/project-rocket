import mqtt_config
import json
from time import sleep

r = mqtt_config.create_mqtt()
r.publish('admin/servicehost', json.dumps({'host': '*', 'operation': 'update'}))
sleep(1)
