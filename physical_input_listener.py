from gc import callbacks
import mqtt_config

class PhysicalInputListener:
    # callbacks is a map of input (enum value) to the function that should be called
    def __init__(self, callbacks):
        self.callbacks = callbacks
        self.subscription = mqtt_config.Subscriber('event/physical_input', self.message_received)

    def message_received(self, msg):
        if msg.input in self.callbacks:
            self.callbacks[msg.input](msg)


