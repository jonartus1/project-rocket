import json
import time

from heartbeat import Heartbeat
from mqtt_config import create_logger, Subscriber, create_mqtt
from lighting_trigger import LightingTrigger, LightingScene
from physical_inputs import PhysicalInputs
from sound_trigger import SoundTrigger, Channels, Pan
from physical_input_listener import PhysicalInputListener

class RocketShipAmbience:
    name = 'RocketShipAmbience'
    
    def __init__(self):
        self.color = {
            'r': 0,
            'g': 0,
            'b': 0
        }

        self.logger = create_logger(self.name, 'RocketShipAmbience')
        self.publisher = create_mqtt()
        
        self.lighting = LightingTrigger()
        self.sound = SoundTrigger()

        callbacks = {
            PhysicalInputs.R_Pot: self.set_r,
            PhysicalInputs.G_Pot: self.set_g,
            PhysicalInputs.B_Pot: self.set_b
        }   
    
        self.listener = PhysicalInputListener(callbacks)    

    def run(self):
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: self.color)
        self.init()

    def init(self):
        self.lighting.turn_everything_off()

        # Turn off music/sound
        self.sound.stop(Channels.main)
        self.sound.stop_music(Channels.main)

        self.lighting.play(LightingScene.CurtainWarmer)

    def set_r(self, msg):
        self.color['r'] = msg.value
        self.publish_updates()

    def set_g(self, msg):
        self.color['g'] = msg.value
        self.publish_updates()

    def set_b(self, msg):
        self.color['b'] = msg.value
        self.publish_updates()

    def publish_updates(self):
        print("Setting main lights: " + json.dumps(self.color))
        self.lighting.set_intensity(LightingScene.AmbientRed, self.color['r'])
        self.lighting.set_intensity(LightingScene.AmbientGreen, self.color['g'])
        self.lighting.set_intensity(LightingScene.AmbientBlue, self.color['b'])
        self.publisher.publish('ship/main_lights/set', json.dumps(self.color))
        

if __name__ == "__main__":
    s = RocketShipAmbience()
    s.run()

    while True:
        time.sleep(60)

