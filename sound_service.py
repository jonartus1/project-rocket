import os
import sys
import traceback
import mqtt_config
import global_config
import pygame
import time
from espeak import espeak
from sound_trigger import Operations, Pan
from heartbeat import Heartbeat


class SoundService:
    def __init__(self):
        self.config = global_config.get_service_config('sound_service')
        self.channel = self.config['channel']
        self.mqtt_topic = "commands/service/sound." + self.channel

        self.logger = mqtt_config.create_logger("sound")        
        self.logger.info(
            'Starting sound service on {} with subscription to {}'.format(global_config.get_hostname(), self.channel))

        pygame.mixer.pre_init(44100, 16, 2, 4096)
        pygame.init()

        # We seem to need a PyGame screen before it makes any noise...
        self.screen = pygame.display.set_mode((400, 400), 0, 32)
        self.heartbeat = Heartbeat(self.__class__.__name__, get_state=lambda: {
            'state': 'running',
            'channel': self.channel
        })

    def run(self):
        self.sub = mqtt_config.Subscriber(self.mqtt_topic, self.message_received)

    def message_received(self, msg):
        print (msg.get_raw())

        if msg.operation == Operations.play.name:
            self.play_sound(msg)
        elif msg.operation == Operations.stop.name:
            pygame.mixer.stop()
        elif msg.operation == Operations.speak.name:
            self.speak(msg)
        elif msg.operation == Operations.play_music.name:
            self.play_music(msg)
        elif msg.operation == Operations.stop_music.name:
            pygame.mixer.music.stop()

    def play_sound(self, req):
        try:
            self.logger.info('Playing ' + req.filename)

            # We need to get a channel first so that we can set the pan in the case of mono sounds.
            channel = pygame.mixer.find_channel()

            if channel is None:
                self.logger.warn("Attempt to play sound {} with no channel.".format(req.filename))
                return

            volume = float(req.volume)

            if req.pan == Pan.both.name:
                channel.set_volume(volume)
            elif req.pan == Pan.left.name:
                channel.set_volume(volume, 0.0)
            elif req.pan == Pan.left.name:
                channel.set_volume(0.0, volume)
            else:
                raise ValueError('Unrecognised pan value.')

            sample = pygame.mixer.Sound(os.path.join('/home/pi/project-kurtis/media', req.filename + '.wav'))
            channel.play(sample, loops=req.loops)
        except:
            self.logger.exception('Error playing sound ' + req.filename)

    def speak(self, req):
        try:
            self.logger.info('Saying: ' + req.text)
            espeak.synth(req.text)
        except:
            self.logger.exception('Error speaking ' + req.text)

    def play_music(self, req):
        try:
            self.logger.info('Playing music ' + req.filename)

            volume = float(req.volume)
            pygame.mixer.music.set_volume(volume)
            full_filename = os.path.join('/home/jon/scape/media', req.filename + '.mp3')
            pygame.mixer.music.load(full_filename)
            pygame.mixer.music.play(100)
        except:
            self.logger.exception('Error playing sound ' + req.filename)


if __name__ == "__main__":
    sound = SoundService()
    sound.run()

    while True:
        time.sleep(60)
