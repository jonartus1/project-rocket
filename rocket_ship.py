import time

from physical_input_listener import PhysicalInputListener
from heartbeat import Heartbeat
from transitions import Machine
from mqtt_config import create_logger, Subscriber, create_mqtt
from global_config import get_variable
from lighting_trigger import LightingTrigger, LightingScene
from state_trigger import StateTrigger
from sound_trigger import SoundTrigger, Channels, Pan

class RocketShip:
    states = ['deactivated', 'activated']
    name = 'rocket_ship'

    def __init__(self):
        self.logger = create_logger(self.name)
        self.machine = Machine(model=self, states=RocketShip.states, initial='deactivated',
                               before_state_change='before_change', after_state_change='after_change')

        self.machine.add_transition(trigger='activate', source='deactivated', dest='activated', before='on_activate')
        self.machine.add_transition(trigger='deactivate', source='activated', dest='deactivated', before='on_deactivate')
        
        self.publisher = create_mqtt()
        
        self.lighting = LightingTrigger()
        self.sound = SoundTrigger()
        
        self.state_trigger = StateTrigger()
        self.state_trigger.broadcast_state_change(self.name, self.state)  # Let everything else know the initial state.
        
        self.heartbeat = Heartbeat(self.name, get_state=lambda: self.state)
        
        self.known_uids = get_variable('known_uids')
        self.init()

    def before_change(self):
        msg = 'Before transition - state: {}'.format(self.state)
        print(msg)
        self.logger.info(msg)

    def after_change(self):
        msg = 'After transition - state: {}'.format(self.state)        
        print(msg)        
        self.logger.info(msg)

        self.state_trigger.broadcast_state_change(self.name, self.state)

    def rfid_scanned(self, msg):
        id = msg.uid

        if id in self.known_uids:
            name = self.known_uids[id]['name']
            self.sound.speak(Channels.main, "Welcome " + name + ". Activating.")
            self.activate()

    def deactivate_pushed(self, msg):
        print("deactivate_pushed")
        self.deactivate()

    def on_activate(self):
        print("on_activate called")
        self.lighting.scene(LightingScene.CurtainWarmer, 0)
        self.lighting.scene(LightingScene.NormalRunning)
        # self.sound.play_music(Channels.main, )

    def on_deactivate(self):
        print("on_deactivate called")
        self.lighting.scene(LightingScene.NormalRunning, 0)
        self.lighting.scene(LightingScene.CurtainWarmer, 255)
        # self.sound.play_music(Channels.main, )

    def run(self):
        self.rfid_sub = Subscriber('events/user/rfid', self.rfid_scanned)
        self.logout_sub = PhysicalInputListener({ 20: self.deactivate_pushed })
                

    def init(self):
        # Turn off music/sound
        self.sound.stop(Channels.main)
        self.sound.stop_music(Channels.main)

        self.lighting.scene(LightingScene.CurtainWarmer)
        


if __name__ == "__main__":
    s = RocketShip()
    s.run()

    while True:
        time.sleep(60)

