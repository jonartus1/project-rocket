from threading import Thread
import time
import mqtt_config
from lighting_trigger import LightingTrigger, LightingScene
from lighting_service import WledInterface, MqttInterface

# t = LightingTrigger()
# t.scene(LightingScene.NormalRunning, 255)

# while True:
#     time.sleep(60)

i = MqttInterface()
scene = { 'r': 64, 'g': 128, 'b': 192 }
i.set_scene(scene)
time.sleep(10)