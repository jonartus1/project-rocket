import mqtt_config
import json
from enum import Enum


class StateChanges(Enum):
    start = 1
    puzzle_activated = 2
    puzzle_deactivated = 3
    puzzle_completed = 4
    puzzle_reset = 5
    puzzle_force_complete = 6


class StateTrigger:
    def __init__(self):
        self.mqtt = mqtt_config.create_mqtt()

    def activate_puzzle(self, puzzle_name):
        req = {'scope': 'puzzle', 'event': StateChanges.puzzle_activated.name, 'args': {'puzzle_name': puzzle_name}}
        self.mqtt.publish('state', json.dumps(req))

    def complete_puzzle(self, puzzle_name):
        req = {'scope': 'puzzle', 'event': StateChanges.puzzle_completed.name, 'args': {'puzzle_name': puzzle_name}}
        self.mqtt.publish('state', json.dumps(req))

    def reset_puzzle(self, puzzle_name):
        req = {'scope': 'puzzle', 'event': StateChanges.puzzle_reset.name, 'args': {'puzzle_name': puzzle_name}}
        self.mqtt.publish('state', json.dumps(req))

    def broadcast_state_change(self, object_name, new_state):
        req = {'new_state': new_state}
        self.mqtt.publish("system/" + object_name + "/state_changed", json.dumps(req))

    def force_complete_puzzle(self, puzzle_name):
        req = {'scope': 'puzzle', 'event': StateChanges.puzzle_force_complete.name, 'args': {'puzzle_name': puzzle_name}}
        self.mqtt.publish('state', json.dumps(req))

