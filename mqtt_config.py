from pydoc import cli
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import logging
import mqtt_handlers
import json
from global_config import get_config_section, get_variable
from global_config import get_hostname


class MqttWrapper:
    def __init__(self, mqtt):
        self.mqtt = mqtt
        self.namespace = get_config_section('global_namespace')

    def publish(self, topic, payload=None, qos=0, retain=False, properties=None):
        ns_topic = f'{self.namespace}/{topic}'
        self.mqtt.publish(ns_topic, payload, qos, retain, properties)
        


def create_mqtt(on_connect=None, on_subscribe=None, on_message=None):
    mqtt_config = get_config_section('mqtt')
    mqtt_host = mqtt_config['host']
    
    print(f'Connecting to MQTT for host {mqtt_host}')
    client = mqtt.Client()

    if on_connect:
        client.on_connect = on_connect

    if on_subscribe:
        client.on_subscribe = on_subscribe

    if on_message:
        client.on_message = on_message

    if 'username' in mqtt_config:
        client.username_pw_set(mqtt_config['username'], mqtt_config['password'])

    client.connect(mqtt_host)
    client.loop_start()

    return MqttWrapper(client)
    
def create_logger(namespace, stdout=True, Hardware=False):
    if Hardware:
        channel = f'hardware/{namespace}/logs'
    else:
        channel = f'service/{namespace}/logs'
    print(f'Creating logger for channel: {channel}')
    
    format_string='%(asctime)-15s %(name)-20s %(levelname)8s  %(message)s'
    logging.basicConfig(format=format_string)

    logger = logging.getLogger(channel)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(mqtt_handlers.MqttHandler(channel, create_mqtt()))

    if stdout:
        stdout_logger = logging.StreamHandler()
        stdout_logger.setFormatter(logging.Formatter(format_string))
        logger.addHandler(stdout_logger)

    logger.propagate = False
    return logger


class Payload:
    def __init__(self, raw):
        if len(raw) > 0:
            self.__dict__ = json.loads(raw)

        self.raw = raw or 'No raw payload...'

    def has_args(self):
        return hasattr(self, 'args')

    def get_raw(self):
        return self.raw

class Subscriber:
    def on_connect(self, client, userdata, flags, rc):
        print(f"Connected with result code {rc}, subcribing to topic {self.topic}")
        client.subscribe(self.topic)

    def on_subscribe(self, client, userdata, mid, granted_qos):
        print(f"Subscribed to {self.topic} with Result: {mid}, QoS: {granted_qos}")
    
    def on_message(self, client, userdata, msg):
        try:
            payload = Payload(msg.payload)
            self.callback(payload)
        except Exception as e:
            print(e)

    def __init__(self, topic, callback):
        namespace = get_config_section('global_namespace')

        self.callback = callback
        self.topic = f'{namespace}/{topic}'
        self.client = create_mqtt(self.on_connect, self.on_subscribe, self.on_message)

           
