#!/bin/bash

echo 'Setting up ' $1

echo 'Updating... Note that this may take ~20 minutes. Do not interrupt the process!'
ssh-keyscan ssh-keyscan -H $1 >> ~/.ssh/known_hosts
ssh-copy-id -i ~/.ssh/id_rsa.pub rocket@$1

ssh rocket@$1 ssh-keygen -q -t rsa -N '' <<< $'\ny' >/dev/null 2>&1
ssh rocket@$1 cat /home/rocket/.ssh/id_rsa.pub
read -n 1 -s -r -p "Copy the public key and add it to Gitlab as a deploy key, then press any key to continue..."

ssh rocket@$1 sudo apt-get update
ssh rocket@$1 sudo apt-get upgrade
ssh rocket@$1 sudo apt-get install vim

ssh-keyscan gitlab.com | ssh rocket@$1 "cat > /home/rocket/.ssh/known_hosts"
ssh rocket@$1 git config --global user.name "Syzygy Corporation"
ssh rocket@$1 git config --global user.email "dev@syzygy.online"
ssh rocket@$1 git clone git@gitlab.com:jonartus1/project-rocket.git /home/rocket/rocket

ssh rocket@$1 "sudo apt-get install espeak python3-espeak"
ssh rocket@$1 "sudo pip3 install paho-mqtt simplejson websocket-client pygame npyscreen transitions"

ssh rocket@$1 "mkdir /home/rocket/.qlcplus"
ssh rocket@$1 "mkdir /home/rocket/.qlcplus/fixtures"
scp ./*.qxf rocket@$1:/home/rocket/.qlcplus/fixtures 
scp ./*.deb rocket@$1:/home/rocket/

ssh rocket@$1 "mkdir /home/rocket/.config/autostart"
scp ./rocket.desktop rocket@$1:/home/rocket/.config/autostart/
ssh rocket@$1 "sudo shutdown -r now"

